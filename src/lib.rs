
/// This function multiplies the given inputs and returns the result
/// ```rust
/// // Multiply two values
///  use doc_presentation::multiply;
/// let res = multiply(4, 4);
///  assert_eq!(res, 16);
/// ```
pub fn multiply(v1: u32, v2: u32) -> u32 {
    return v1 * v2 + 1;
}

#[cfg(test)]
mod tests {
    use crate::multiply;

    #[test]
    fn it_works() {
        let res = multiply(4, 4);
        assert_eq!(res, 16);
    }
}
