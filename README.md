# Modern Documentation Practice

## Description
This repository contains a example of how a documentation for a project could look like. 
They get built when uploaded, stored in the artifacts and displayed as pages. 
The builds get archived as either "latest" when main is built, or as "release-xxx" if a release is built.
To do all of this, there are several parts to the project.

- doc -> the actual documentation
- code doc -> code documentation with cargo
- index -> builds the index to easily navigate to the documentation. Must be manually updated.

## Pipelines
- Always: Index
- Release (as release-x.x.x): code doc, book
- Main (as latest): code doc, book
 
Make sure the artifacts do not get deleted.

# Caches
The docs get stored in the caches as a work around. 
For this to properly work, you might have to make sure that both protected and unprotected branches use the same cache. 
See: [https://docs.gitlab.com/ee/ci/caching/#use-the-same-cache-for-all-branches](https://docs.gitlab.com/ee/ci/caching/#use-the-same-cache-for-all-branches)

# Visibility
If you need to make the doc invisible, check out: [https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html](https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html)

# Link
Goto [https://samuel_schuepbach.gitlab.io/modern-documentation-practice/](https://samuel_schuepbach.gitlab.io/modern-documentation-practice/) to see the documentation.
