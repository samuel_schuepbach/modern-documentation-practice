# Start a new documentation

Run the following in bash (Or where ever):
```bash
mdbook init my-first-book
```

Its ready to use now. Open it with:
```bash
mdbook serve --open
```

For more, check at this documentation itself.

# Editor
Basically any text editor or IDE.
- Visual studio code
- CLion
- vim
- ...

## Editor support
Depending on tool used. Sphinx for example has a visual studio code plugin. 

# Features
## Supported
- All standard Markdown features
- - Images
- - Tables
- - Headers
- - Links
- - ...
- Themes
- Search

## Not yet supported
- Formulas
- - Supported by sphinx (maybe mkdocs)
- Pdf export
- - Supported by spinx, mkdocs
