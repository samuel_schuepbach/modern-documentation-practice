# Code documentation and tests
There are convenient tools such as the known doxygen to produce nice looking docs of the API.
Newer doc tools also support doc tests!

## Advantage
- Quite often the API doc is outdated and wouldn't work in the same way anymore.
- Gives additional security and checks
- Produces nice looking documentation


## Example
Add a comment and a little example of how to use the function.

Test your code:
```rust
cargo test --all
```

Generate the documentation:
```rust
cargo doc
```

[The result!](../doc/doc_presentation/index.html)
