# Summary

- [Introduction](./introduction.md)
- [An example with mdbook](./mdbook.md)
- [Documentation structure](./structure.md)
- [Versioning, building, testing and hosting](./doc_as_code.md)
- [Code documentation and tests](./code_doc.md)
