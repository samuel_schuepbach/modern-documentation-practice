# Documentation structure

## Overview
![Overview](./images/documentation_overview.png)

## Explanation
### Code doc
Contains the documentation of the code and the API. This is usually outside of the scope of mdbook or the other tools but ties into them quite well. 
This is usually done with tools such as [doxygen](https://www.doxygen.nl/index.html) for example.

## Project doc
Contains documentation about the project. It should be "close to the code" so that the hurdle to edit the doc is as small as possible. 
Ideally it should live in the same repository as the code. This will also include it in code searches for example

### Directory structure
- Your cool lib (Repository)
- - src
- - - your code
- - doc
- - - your documentation

### Example
For example the documentation of the se-scope-target. It would include documentation such as the framing protocol, architecture and build instructions.

## Product doc

### Directory structure
- Your cool lib (Repository)
- - src
- - - your code
- - doc
- - - your documentation
- Your cool product (Repository) -> doc

### Example
Contains the documentation of the whole se-scope project. For example the texts of the website, the licenses, the overall architecture, ...
