# Introduction

## Often used documentation tools
- Confluence
- Word
- Wikis

## Drawbacks
- Often complicated
- "Far away" from the project
- Scattered in a lot of places 
- Often outdated 
- Version and change tracking is difficult or impossible
- Hard to make them look good
- Often hard to navigate
- The code itself is usually not documented

## Emerging documentation tools
- Usually based on markdown on restructured text
- Compile to html or pdf 
- Can be treated and tested like code
- Can be versioned and tracked by git
- Close to the code
- Can be hosted and versioned
- Usually support themes
- Support plugins for various things

## Examples
- [mdbook](https://rust-lang.github.io/mdBook/) -> [Example](https://rust-lang.github.io/mdBook/)
- [mkdocs](https://www.mkdocs.org/getting-started/) -> [Example](https://www.mkdocs.org/user-guide/writing-your-docs/)
- [sphinx](https://www.sphinx-doc.org/en/master/) -> [Example](https://pythonhosted.org/director/)
- many others

### mdbook
Lightweight and Markdown based. Rather new and not quite as feature rich yet but fast.

### MkDocs
More feature rich, written in Python and Markdown based.

### Sphinx 
Most feature rich of the three. Written in Python and based on Restructured Text.
