# Versioning, building, testing and hosting
The idea is that the documentation can be treated as code. This includes it can be built by the pipeline, tested, versioned and hosted all automatically. 

![doc_pipeline](./images/documentation_pipeline.png)

# Steps

## Build
Automatically compiles the given documentation

## Tests
Depending on tool used, those tests can include unit tests, test for broken links, spell checkers, usw... 

## Versioning
Can automatically be versioned depending on git tag

## Publish
Publish the doc either as pdf (if supported) or host them on a server where its easily accessible

# Read the docs
You might not want to setup all of this on your own. If not, [Read the Docs](https://readthedocs.org/) can be used. 
It can be connected to Github/Bitbucket/Gitlab and automatically builds, hosts and versions the documentation.
[Example](https://readthedocs.org/projects/example/)

# Overview of tools and setups
![publish_pipeline](./images/publish_pipeline.png)
