# Project documentation index
## Latest
- Documentation: [https://samuel_schuepbach.gitlab.io/modern-documentation-practice/latest/book/introduction.html](https://samuel_schuepbach.gitlab.io/modern-documentation-practice/latest/book/introduction.html)
- Code: [https://samuel_schuepbach.gitlab.io/modern-documentation-practice/latest/doc/doc_presentation/](https://samuel_schuepbach.gitlab.io/modern-documentation-practice/latest/doc/doc_presentation/)

## Releases
### release-v0.1.0
- Documentation: [https://samuel_schuepbach.gitlab.io/modern-documentation-practice/release/release-v0.1.0/book/introduction.html](https://samuel_schuepbach.gitlab.io/modern-documentation-practice/release/release-v0.1.0/book/introduction.html)
- Code: [https://samuel_schuepbach.gitlab.io/modern-documentation-practice/release/release-v0.1.0/doc/doc_presentation/](https://samuel_schuepbach.gitlab.io/modern-documentation-practice/release/release-v0.1.0/doc/doc_presentation/)
